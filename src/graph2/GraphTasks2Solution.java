package graph2;

import java.util.*;
import java.util.stream.IntStream;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        boolean[] CheckTheTop = new boolean[adjacencyMatrix.length];
        CheckTheTop[startIndex] = true;
        IntStream.range(0, adjacencyMatrix.length).filter(j -> j != startIndex).forEachOrdered(j -> {
            if (adjacencyMatrix[startIndex][j] == 0) hashMap.put(j, Integer.MAX_VALUE);
            else hashMap.put(j, adjacencyMatrix[startIndex][j]);
        });
        for (int x : hashMap.keySet())
            for (var j = 0; j < adjacencyMatrix.length; j++) {
                if (adjacencyMatrix[x][j] == 0) continue;
                if (j != x && !CheckTheTop[j] && adjacencyMatrix[x][j] + hashMap.get(x) < hashMap.get(j)) {
                    hashMap.put(j, adjacencyMatrix[x][j] + hashMap.get(x));
                    CheckTheTop[j] = true;
                }
            }
        return hashMap;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        HashSet<Integer> setOfVertices = new HashSet<>();
        int sum = 0;
        setOfVertices.add(0);
        int min = adjacencyMatrix[0][1];
        int minIndex = 1;
        for (int i = 1; i < adjacencyMatrix.length; i++) {
            if (adjacencyMatrix[0][i] < min && adjacencyMatrix[0][i] > 0) {
                min = adjacencyMatrix[0][i];
                minIndex = i;
            }
        }
        setOfVertices.add(minIndex);
        sum += min;
        for (int i = 0; i < adjacencyMatrix.length - 2; i++) {
            int queriedIndex = 0;
            int minValue = Integer.MAX_VALUE;
            for (Integer x : setOfVertices) {
                for (int j = 0; j < adjacencyMatrix.length; j++) {
                    if (adjacencyMatrix[x][j] < minValue && adjacencyMatrix[x][j] > 0 && !setOfVertices.contains(j)) {
                        minValue = adjacencyMatrix[x][j];
                        queriedIndex = j;
                    }
                }
            }
            setOfVertices.add(queriedIndex);
            sum += minValue;
        }
        return sum;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        List<Set<Integer>> listOfPickedVertex = new ArrayList<>();
        TreeMap<Integer, List<Set<Integer>>> treeOfMap = new TreeMap<>();
        int key;
        for (int i = 0; i < adjacencyMatrix.length; i++)
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                Set<Integer> pair = new HashSet<>();
                key = adjacencyMatrix[i][j];
                if (key <= 0) {
                    continue;
                }
                pair.add(i);
                pair.add(j);

                if (!treeOfMap.containsKey(key)){
                    treeOfMap.put(key, new ArrayList<>());
                }

                boolean check = true;
                for (Set<Integer> setForCheck : treeOfMap.get(key))
                    if (setForCheck.containsAll(pair)) check = false;
                if (check) treeOfMap.get(key).add(pair);
            }


        int sum = 0;
        for (Integer r : treeOfMap.keySet()) {
            int firstEntry = -1;
            int secondEntry = -1;

            for (Set<Integer> valueSet : treeOfMap.get(r)) {
                if (listOfPickedVertex.size() == 0) {
                    listOfPickedVertex.add(new HashSet<>(valueSet));
                    sum += r;
                    continue;
                }
                Object[] objects = valueSet.toArray();
                for (int i = 0; i < objects.length; i++) {
                    for (int k = 0; k < listOfPickedVertex.size(); k++) {
                        if (listOfPickedVertex.get(k).contains((Integer) objects[i])) {
                            if (i == 0) firstEntry = k;
                            if (i == 1) secondEntry = k;
                        }

                    }
                }
                if (firstEntry == secondEntry && firstEntry >= 0){
                    continue;
                }

                if (firstEntry >= 0 && secondEntry < 0) {
                    listOfPickedVertex.get(firstEntry).addAll(valueSet);
                }

                if (secondEntry >= 0 && firstEntry < 0) {
                    listOfPickedVertex.get(secondEntry).addAll(valueSet);
                }

                if (firstEntry >= 0 && secondEntry >= 0) {
                    listOfPickedVertex.get(firstEntry).addAll(listOfPickedVertex.get(secondEntry));
                    listOfPickedVertex.get(secondEntry).clear();
                }
                if (firstEntry < 0 && secondEntry < 0) {
                    listOfPickedVertex.add(valueSet);
                }
                firstEntry = -1;
                secondEntry = -1;
                sum += r;
            }
        }
        return sum;
    }
}
